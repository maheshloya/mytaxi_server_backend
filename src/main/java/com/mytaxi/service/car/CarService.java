package com.mytaxi.service.car;

import com.mytaxi.domainobject.CarDO;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

public interface CarService
{

    CarDO create(CarDO car) throws ConstraintsViolationException, EntityNotFoundException;


    CarDO find(Long carId) throws EntityNotFoundException;


    List<CarDO> findAll() throws EntityNotFoundException;


    CarDO update(Long carId, CarDO updatedCarDO) throws EntityNotFoundException;


    void delete(Long cardId) throws EntityNotFoundException;
}
