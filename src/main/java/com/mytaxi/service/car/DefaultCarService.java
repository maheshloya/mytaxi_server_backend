package com.mytaxi.service.car;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;

@Service
public class DefaultCarService implements CarService
{

    private static Logger LOG = LoggerFactory.getLogger(DefaultCarService.class);

    private CarRepository carRepository;


    @Autowired
    public DefaultCarService(CarRepository carRepository)
    {
        this.carRepository = carRepository;

    }


    @Override
    public CarDO create(CarDO car) throws ConstraintsViolationException, EntityNotFoundException
    {

        try
        {
            return carRepository.save(car);

        }
        catch (DataIntegrityViolationException dataIntegrtityException)
        {
            LOG.warn("Constraints voilated during creating new car entry.", dataIntegrtityException);
            throw new ConstraintsViolationException(dataIntegrtityException.getMessage());
        }
    }


    @Override
    public CarDO find(Long carId) throws EntityNotFoundException
    {

        CarDO carDO = carRepository.findById(carId).get();

        if (carDO == null)
        {
            throw new EntityNotFoundException("Car with id:" + carId + "could not be found!");
        }
        return carDO;
    }


    @Override
    public List<CarDO> findAll() throws EntityNotFoundException
    {
        return StreamSupport.stream(carRepository.findAll().spliterator(), true).collect(Collectors.toList());
    }


    @Override
    @Transactional
    public CarDO update(Long carId, CarDO updatedCarDO) throws EntityNotFoundException
    {

        CarDO carDO = this.find(carId);

        updateCarInformation(updatedCarDO, carDO);

        return carDO;
    }


    @Override
    @Transactional
    public void delete(Long cardId) throws EntityNotFoundException
    {

        CarDO carDO = this.find(cardId);
        carDO.setDeleted(Boolean.TRUE);
    }


    private void updateCarInformation(CarDO updatedCarDO, CarDO carDO) throws EntityNotFoundException
    {

        carDO.setRating(updatedCarDO.getRating());
        carDO.setConvertible(updatedCarDO.getConvertible());
        carDO.setLicensePlate(updatedCarDO.getLicensePlate());
        carDO.setEngineType(updatedCarDO.getEngineType());
        carDO.setSeatCount(updatedCarDO.getSeatCount());
        carDO.setManufacturer(updatedCarDO.getManufacturer());

    }

}
