package com.mytaxi.dataaccessobject;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mytaxi.domainobject.CarWithDriverDO;
import com.mytaxi.domainobject.DriverDO;

@Repository
public interface CarWithDriverRepository extends CrudRepository<CarWithDriverDO, Long>
{
    @Query("SELECT cwd FROM CarWithDriverDO cwd where cwd.driverId = :driverId AND cwd.carId = :carId")
    CarWithDriverDO findByDriverIdAndCarId(@Param("driverId") final Long driverId, @Param("carId") final Long carId);


    @Query("SELECT cwd FROM CarWithDriverDO cwd where cwd.carId = :carId")
    CarWithDriverDO findByCarId(@Param("carId") final Long carId);


    @Query("select driver FROM CarDO car, DriverDO driver ,CarWithDriverDO cwd "
        +
        "WHERE cwd.carId = car.id AND cwd.driverId = driver.id " +
        "AND " +
        " (:seatCount = '' or car.seatCount like '%:seatCount%')" + "and" +
        " (:convertible = '' or car.convertible like '%:convertible%')" + "and" +
        " (:rating = '' or car.rating like '%:rating%')" + "and" +
        " (:engineType = '' or car.engineType like '%:engineType%')" + "and" +
        " (:manufacturer = '' or car.manufacturer like '%:manufacturer%')")
    List<Object[]> searchDriversByCarAttributes(
        @Param("seatCount") int seatCount,
        @Param("convertible") boolean convertible,
        @Param("rating") float rating,
        @Param("engineType") String engineType,
        @Param("manufacturer") String manufacturer);

}
