package com.mytaxi.security;

import static java.util.Collections.emptyList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mytaxi.dataaccessobject.DriverRepository;
import com.mytaxi.domainobject.DriverDO;

@Service
public class DefaultUserDetailsService implements UserDetailsService
{
    private final DriverRepository driverRepository;


    @Autowired
    public DefaultUserDetailsService(DriverRepository driverRepository)
    {
        this.driverRepository = driverRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        DriverDO driverDO = driverRepository.findByUsername(username);
        if (driverDO == null)
        {
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(driverDO.getUsername(), driverDO.getPassword(), emptyList());
    }
}
