package com.mytaxi.security;

public interface SecurityConstants
{
    String SECRET = "SomeSecret";
    long EXPIRATION_TIME = 86400;
    String TOKEN_PREFIX = "Bearer ";
    String HEADER_STRING = "Authorization";
}
