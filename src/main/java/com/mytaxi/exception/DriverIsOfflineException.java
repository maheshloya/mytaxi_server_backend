package com.mytaxi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Driver Is OFFLINE")

public class DriverIsOfflineException extends Exception
{

    public DriverIsOfflineException(String message)
    {
        super(message);
    }
}
