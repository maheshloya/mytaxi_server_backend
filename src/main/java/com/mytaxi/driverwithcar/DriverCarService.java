package com.mytaxi.driverwithcar;

import java.util.List;
import java.util.Map;

import com.mytaxi.datatransferobject.DriverCarDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.DriverIsOfflineException;
import com.mytaxi.exception.EntityNotFoundException;

public interface DriverCarService
{

    DriverCarDTO selectCar(Long driverId, Long carId) throws EntityNotFoundException, DriverIsOfflineException, CarAlreadyInUseException;


    void deSelectCar(Long driverId, Long carId) throws EntityNotFoundException;


    List<DriverDO> searchDriversByCarAttributes(Map<String, Object> params);
}
