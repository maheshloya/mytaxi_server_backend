package com.mytaxi.driverwithcar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mytaxi.dataaccessobject.CarWithDriverRepository;
import com.mytaxi.datatransferobject.DriverCarDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.CarWithDriverDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.DriverIsOfflineException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.car.CarService;
import com.mytaxi.service.driver.DriverService;

@Service
public class DefaultDriverCarService implements DriverCarService
{

    private DriverService driverService;
    private CarWithDriverRepository carWithDriverRepository;
    private CarService carService;


    @Autowired
    public DefaultDriverCarService(
        DriverService driverService, CarService carService,
        CarWithDriverRepository carWithDriverRepository)
    {

        this.driverService = driverService;
        this.carService = carService;
        this.carWithDriverRepository = carWithDriverRepository;
    }


    @Override
    @Transactional
    public DriverCarDTO selectCar(Long driverId, Long carId)
        throws EntityNotFoundException, DriverIsOfflineException, CarAlreadyInUseException
    {

        CarWithDriverDO driverCarDOFound = carWithDriverRepository.findByDriverIdAndCarId(driverId, carId);

        if (driverCarDOFound == null)
        {

            CarDO carDO = findAvailableCar(carId);
            DriverDO driverDO = findOnlineDriver(driverId);

            CarWithDriverDO savedDriverCarDO = saveDriverCarDOToDB(carDO, driverDO);
            return getDriverCarDTOFromDO(savedDriverCarDO);

        }
        else
        {
            return getDriverCarDTOFromDO(driverCarDOFound);
        }
    }


    private CarDO findAvailableCar(Long carId) throws EntityNotFoundException, CarAlreadyInUseException
    {
        CarDO carDO = carService.find(carId);
        CarWithDriverDO foundCar = carWithDriverRepository.findByCarId(carId);

        if (foundCar != null)
        {
            throw new CarAlreadyInUseException("Requested car is already taken");
        }
        return carDO;
    }


    private DriverDO findOnlineDriver(Long driverId) throws EntityNotFoundException, DriverIsOfflineException
    {
        DriverDO driverDO = driverService.find(driverId);

        if (driverDO.getOnlineStatus() == OnlineStatus.OFFLINE)
        {
            throw new DriverIsOfflineException(
                "Driver with id :" + driverId + " is OFFLINE and hence cannot select the car");
        }
        return driverDO;
    }


    @Override
    @Transactional
    public void deSelectCar(Long driverId, Long carId) throws EntityNotFoundException
    {

        CarWithDriverDO driverCarDOFound = carWithDriverRepository.findByDriverIdAndCarId(driverId, carId);

        if (driverCarDOFound == null)
        {
            throw new EntityNotFoundException(
                "Could not find entity with driverid:" + driverId + " and carid:" + carId);
        }
        else
        {
            carWithDriverRepository.delete(driverCarDOFound);
        }

    }


    private DriverCarDTO getDriverCarDTOFromDO(CarWithDriverDO savedDriverCarDO)
    {
        return DriverCarDTO
            .newBuilder().setDriverId(savedDriverCarDO.getDriverId())
            .setCarId(savedDriverCarDO.getCarId()).build();
    }


    private CarWithDriverDO saveDriverCarDOToDB(CarDO carDO, DriverDO driverDO)
    {
        CarWithDriverDO carWithDriverDO = new CarWithDriverDO(driverDO.getId(), carDO.getId());

        return carWithDriverRepository.save(carWithDriverDO);
    }


    private List<DriverDO> searchDriversByCarAttributes(
        int seatCount, boolean convertible, float rating,
        String engineType, String manufacturer)
    {
        List<DriverDO> driverDOList = new ArrayList<>();

        List<Object[]> driversByCarAttributes =
            carWithDriverRepository.searchDriversByCarAttributes(
                seatCount,
                convertible, rating, engineType, manufacturer);

        driversByCarAttributes.forEach((objectArr) -> {
            DriverDO driver = (DriverDO) objectArr[0];
            driverDOList.add(driver);
        });

        return driverDOList;
    }


    @Override
    public List<DriverDO> searchDriversByCarAttributes(Map<String, Object> params)
    {
        int seatCount = (int) params.getOrDefault("seatCount", "");
        boolean convertible = (boolean) params.getOrDefault("convertible", "");
        float rating = (float) params.getOrDefault("rating", "");
        String engineType = (String) params.getOrDefault("engineType", "");
        String manufacturer = (String) params.getOrDefault("manufacturer", "");

        return this.searchDriversByCarAttributes(seatCount, convertible, rating, engineType, manufacturer);

    }

}
