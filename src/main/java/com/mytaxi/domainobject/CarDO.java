package com.mytaxi.domainobject;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(
    name = "car",
    uniqueConstraints = @UniqueConstraint(name = "uc_license_plate", columnNames = {"license_plate"}))
public class CarDO
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = "license_plate")
    @NotNull(message = "License plate cannot be null!")
    private String licensePlate;

    @Column(nullable = false, name = "seat_count")
    @NotNull(message = "Seat count cannot be null!")
    private Integer seatCount;

    @Column
    private Boolean convertible;

    @Column
    private Float rating;

    @Column(nullable = false, name = "engine_type")
    @NotNull(message = "Engine type cannot be null!")
    private String engineType;

    @Column(nullable = false, name = "date_created")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime dateCreated = ZonedDateTime.now();

    @Column(nullable = false, name = "manufacturer")
    @NotNull(message = "Manufacturer name cannot be null")
    private String manufacturer;

    @Column(nullable = false)
    private Boolean deleted = false;


    private CarDO()
    {}


    public CarDO(
        String licensePlate,
        Integer seatCount,
        Boolean convertible,
        Float rating,
        String engineType,
        String manufacturer)
    {
        this.licensePlate = licensePlate;
        this.seatCount = seatCount;
        this.convertible = convertible;
        this.rating = rating;
        this.engineType = engineType;
        this.manufacturer = manufacturer;
    }


    public void setId(Long id)
    {
        this.id = id;
    }


    public Long getId()
    {
        return id;
    }


    public String getLicensePlate()
    {
        return licensePlate;
    }


    public void setLicensePlate(String licensePlate)
    {
        this.licensePlate = licensePlate;
    }


    public Integer getSeatCount()
    {
        return seatCount;
    }


    public void setSeatCount(Integer seatCount)
    {
        this.seatCount = seatCount;
    }


    public Boolean getConvertible()
    {
        return convertible;
    }


    public void setConvertible(Boolean convertible)
    {
        this.convertible = convertible;
    }


    public Float getRating()
    {
        return rating;
    }


    public void setRating(Float rating)
    {
        this.rating = rating;
    }


    public String getEngineType()
    {
        return engineType;
    }


    public void setEngineType(String engineType)
    {
        this.engineType = engineType;
    }


    public ZonedDateTime getDateCreated()
    {
        return dateCreated;
    }


    public String getManufacturer()
    {
        return manufacturer;
    }


    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }


    public Boolean getDeleted()
    {
        return deleted;
    }


    public void setDeleted(Boolean deleted)
    {
        this.deleted = deleted;
    }


    @Override
    public String toString()
    {
        return "CarDO{"
            +
            "id=" + id +
            ", licensePlate='" + licensePlate + '\'' +
            ", seatCount=" + seatCount +
            ", convertible=" + convertible +
            ", rating=" + rating +
            ", engineType='" + engineType + '\'' +
            ", dateCreated=" + dateCreated +
            ", manufacturer=" + manufacturer +
            ", deleted=" + deleted +
            '}';
    }

    @Component
    public static class CarDOBuilder
    {

        private Long id;
        private String licensePlate;
        private Integer seatCount;
        private Boolean convertible;
        private Float rating;
        private String engineType;
        private String manufacturer;


        public CarDO build()
        {
            return new CarDO(
                this.licensePlate,
                this.seatCount,
                this.convertible,
                this.rating,
                this.engineType,
                this.manufacturer);
        }


        public CarDOBuilder setId(Long id)
        {
            this.id = id;
            return this;
        }


        public CarDOBuilder setLicensePlate(String licensePlate)
        {
            this.licensePlate = licensePlate;
            return this;
        }


        public CarDOBuilder setSeatCount(Integer seatCount)
        {
            this.seatCount = seatCount;
            return this;
        }


        public CarDOBuilder setConvertible(Boolean convertible)
        {
            this.convertible = convertible;
            return this;
        }


        public CarDOBuilder setRating(Float rating)
        {
            this.rating = rating;
            return this;
        }


        public CarDOBuilder setEngineType(String engineType)
        {
            this.engineType = engineType;
            return this;
        }


        public CarDOBuilder setManufacturer(String manufacturer)
        {
            this.manufacturer = manufacturer;
            return this;
        }
    }
}
