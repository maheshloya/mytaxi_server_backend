package com.mytaxi.domainobject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
    name = "car_driver",
    uniqueConstraints = {
        @UniqueConstraint(
            columnNames = {"driver_id", "car_id"})
    })
public class CarWithDriverDO
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "driver_id", unique = true)
    private Long driverId;

    @Column(name = "car_id", unique = true)
    private Long carId;


    public CarWithDriverDO(Long driverId, Long carId)
    {
        this.driverId = driverId;
        this.carId = carId;
    }


    public Long getId()
    {
        return id;
    }


    public void setId(Long id)
    {
        this.id = id;
    }


    public Long getDriverId()
    {
        return driverId;
    }


    public void setDriverId(Long driverId)
    {
        this.driverId = driverId;
    }


    public Long getCarId()
    {
        return carId;
    }


    public void setCarId(Long carId)
    {
        this.carId = carId;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        CarWithDriverDO driverCar = (CarWithDriverDO) o;

        return (id != null ? id.equals(driverCar.id) : driverCar.id == null) && driverId.equals(driverCar.driverId) && carId.equals(driverCar.carId);
    }


    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + driverId.hashCode();
        result = 31 * result + carId.hashCode();
        return result;
    }
}
