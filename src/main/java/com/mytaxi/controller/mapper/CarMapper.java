package com.mytaxi.controller.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;

@Component
public class CarMapper
{

    public CarDTO makeCarDTO(CarDO carDO)
    {
        CarDTO carDTO =
            CarDTO
                .newBuilder().setId(carDO.getId()).setLicensePlate(carDO.getLicensePlate())
                .setSeatCount(carDO.getSeatCount()).setConvertible(carDO.getConvertible()).setRating(carDO.getRating())
                .setEngineType(carDO.getEngineType()).setManufacturer(carDO.getManufacturer()).build();

        return carDTO;
    }


    public CarDO makeCarDO(CarDTO carDTO)
    {

        CarDO carDO =
            new CarDO.CarDOBuilder()
                .setId(carDTO.getId()).setLicensePlate(carDTO.getLicensePlate())
                .setSeatCount(carDTO.getSeatCount()).setConvertible(carDTO.getConvertible())
                .setRating(carDTO.getRating()).setEngineType(carDTO.getEngineType())
                .setManufacturer(carDTO.getManufacturer()).build();
        return carDO;
    }


    public List<CarDTO> makeCarDTO(List<CarDO> listOfCarDO)
    {

        return listOfCarDO.parallelStream().map(carDO -> makeCarDTO(carDO)).collect(Collectors.toList());

    }
}
