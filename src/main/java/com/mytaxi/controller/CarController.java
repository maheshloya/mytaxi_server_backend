package com.mytaxi.controller;

import com.mytaxi.controller.mapper.CarMapper;
import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.car.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping("v1/cars")
public class CarController
{

    private CarService carService;

    private CarMapper carMapper;


    @Autowired
    public CarController(CarService carService, CarMapper carMapper)
    {
        this.carService = carService;
        this.carMapper = carMapper;
    }


    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public CarDTO createDriver(@Valid @RequestBody CarDTO carDTO) throws ConstraintsViolationException, EntityNotFoundException
    {

        CarDO carDO = carMapper.makeCarDO(carDTO);
        return carMapper.makeCarDTO(carService.create(carDO));
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<CarDTO> getAllCars() throws EntityNotFoundException
    {
        return carMapper.makeCarDTO(carService.findAll());
    }


    @GetMapping("/{carId}")
    public CarDTO getCar(@Valid @PathVariable long carId) throws EntityNotFoundException
    {
        return carMapper.makeCarDTO(carService.find(carId));
    }


    @PutMapping("/{carId}")
    @ResponseStatus(HttpStatus.OK)
    public void updateCar(
        @Valid @PathVariable long carId, @Valid @RequestBody CarDTO carDTO)
        throws ConstraintsViolationException, EntityNotFoundException
    {
        carService.update(carId, carMapper.makeCarDO(carDTO));
    }


    @DeleteMapping("/{carId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCar(@Valid @PathVariable long carId) throws EntityNotFoundException
    {
        carService.delete(carId);
    }
}
