package com.mytaxi.datatransferobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarDTO
{

    @JsonIgnore
    private Long id;

    @NotNull(message = "License Plate can not be null!")
    private String licensePlate;

    @NotNull(message = "Seat count can not be null!")
    private Integer seatCount;

    private Boolean convertible;

    private Float rating;

    @NotNull(message = "Manufacturer info can not be null!")
    private String manufacturer;


    private CarDTO()
    {}


    public static CarDTOBuilder newBuilder()
    {
        return new CarDTOBuilder();
    }


    public CarDTO(
        Long id, String licensePlate, Integer seatCount,
        Boolean convertible, Float rating, String engineType,
        String manufacturer)
    {
        this.id = id;
        this.licensePlate = licensePlate;
        this.seatCount = seatCount;
        this.convertible = convertible;
        this.rating = rating;
        this.engineType = engineType;
        this.manufacturer = manufacturer;
    }

    @NotNull(message = "Engine type cannot be null!")
    private String engineType;


    public Long getId()
    {
        return id;
    }


    public void setId(Long id)
    {
        this.id = id;
    }


    public String getLicensePlate()
    {
        return licensePlate;
    }


    public void setLicensePlate(String licensePlate)
    {
        this.licensePlate = licensePlate;
    }


    public Integer getSeatCount()
    {
        return seatCount;
    }


    public void setSeatCount(Integer seatCount)
    {
        this.seatCount = seatCount;
    }


    public Boolean getConvertible()
    {
        return convertible;
    }


    public void setConvertible(Boolean convertible)
    {
        this.convertible = convertible;
    }


    public Float getRating()
    {
        return rating;
    }


    public void setRating(Float rating)
    {
        this.rating = rating;
    }


    public String getEngineType()
    {
        return engineType;
    }


    public void setEngineType(String engineType)
    {
        this.engineType = engineType;
    }


    public String getManufacturer()
    {
        return manufacturer;
    }


    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        CarDTO carDTO = (CarDTO) o;

        if (id != null ? !id.equals(carDTO.id) : carDTO.id != null)
            return false;
        if (!licensePlate.equals(carDTO.licensePlate))
            return false;
        if (!seatCount.equals(carDTO.seatCount))
            return false;
        if (convertible != null ? !convertible.equals(carDTO.convertible) : carDTO.convertible != null)
            return false;
        return (rating != null ? rating.equals(carDTO.rating) : carDTO.rating == null) && manufacturer.equals(carDTO.manufacturer) && engineType.equals(carDTO.engineType);
    }


    @Override
    public int hashCode()
    {
        int result = (id != null ? id.hashCode() : 0);
        result = 31 * result + licensePlate.hashCode();
        result = 31 * result + seatCount.hashCode();
        result = 31 * result + (convertible != null ? convertible.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        result = 31 * result + manufacturer.hashCode();
        result = 31 * result + engineType.hashCode();
        return result;
    }


    @Override
    public String toString()
    {
        return "CarDTO{"
            +
            "id=" + id +
            ", licensePlate='" + licensePlate + '\'' +
            ", seatCount=" + seatCount +
            ", convertible=" + convertible +
            ", rating=" + rating +
            ", manufacturer=" + manufacturer +
            ", engineType='" + engineType + '\'' +
            '}';
    }

    @Component
    public static class CarDTOBuilder
    {

        private Long id;
        private String licensePlate;
        private Integer seatCount;
        private Boolean convertible;
        private Float rating;
        private String engineType;
        private String manufacturer;


        public CarDTO build()
        {
            return new CarDTO(
                this.id,
                this.licensePlate,
                this.seatCount,
                this.convertible,
                this.rating,
                this.engineType,
                this.manufacturer);
        }


        public CarDTOBuilder setId(Long id)
        {
            this.id = id;
            return this;
        }


        public CarDTOBuilder setLicensePlate(String licensePlate)
        {
            this.licensePlate = licensePlate;
            return this;
        }


        public CarDTOBuilder setSeatCount(Integer seatCount)
        {
            this.seatCount = seatCount;
            return this;
        }


        public CarDTOBuilder setConvertible(Boolean convertible)
        {
            this.convertible = convertible;
            return this;
        }


        public CarDTOBuilder setRating(Float rating)
        {
            this.rating = rating;
            return this;
        }


        public CarDTOBuilder setEngineType(String engineType)
        {
            this.engineType = engineType;
            return this;
        }


        public CarDTOBuilder setManufacturer(String manufacturer)
        {
            this.manufacturer = manufacturer;
            return this;
        }
    }
}
