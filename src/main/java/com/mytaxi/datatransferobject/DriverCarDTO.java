package com.mytaxi.datatransferobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DriverCarDTO
{
    @JsonIgnore
    private Long id;

    @NotNull(message = "DriverId can not be null!")
    private Long driverId;

    @NotNull(message = "CarId can not be null!")
    private Long carId;


    private DriverCarDTO()
    {}


    public DriverCarDTO(Long id, Long driverId, Long carId)
    {
        this.id = id;
        this.driverId = driverId;
        this.carId = carId;
    }


    public static DriverCarDTOBuilder newBuilder()
    {
        return new DriverCarDTOBuilder();
    }


    @JsonProperty
    public Long getId()
    {
        return id;
    }


    public void setId(Long id)
    {
        this.id = id;
    }


    public Long getDriverId()
    {
        return driverId;
    }


    public void setDriverId(Long driverId)
    {
        this.driverId = driverId;
    }


    public Long getCarId()
    {
        return carId;
    }


    public void setCarId(Long carId)
    {
        this.carId = carId;
    }

    public static class DriverCarDTOBuilder
    {
        private Long id;
        private Long driverId;
        private Long carId;


        public DriverCarDTOBuilder setId(Long id)
        {
            this.id = id;
            return this;
        }


        public DriverCarDTOBuilder setDriverId(Long driverId)
        {
            this.driverId = driverId;
            return this;
        }


        public DriverCarDTOBuilder setCarId(Long carId)
        {
            this.carId = carId;
            return this;
        }


        public DriverCarDTO build()
        {
            return new DriverCarDTO(id, driverId, carId);
        }

    }
}
